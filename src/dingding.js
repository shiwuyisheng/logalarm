const ChatBot = require("dingtalk-robot-sender");
const config = require('config')
// 直接使用 webhook
const dd_webhook = config.get('dd_webhook')
const dd_key = config.get('dd_key')
const robot = new ChatBot({
  webhook: dd_webhook
});

let textContent = {
  msgtype: "text",
  text: {
    content: "",
  },
  at: {
    atMobiles: [],
    isAtAll: true,
  },
};
module.exports = function message(content) {
  textContent.text.content = `${dd_key}：` + content;
  return robot.send(textContent);
};
