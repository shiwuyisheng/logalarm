const log4js = require('log4js');

log4js.configure({
    replaceConsole: true,
    appenders: {
        stdout: {//控制台输出
            type: 'console',
            layout: {
                type: 'pattern',
                pattern: '[%进程ID:%z] [%用户:%h] [%d{yyyy-MM-dd hh:mm:ss:SSS}] [%p] [%f{1}:%l] - %m'
            }
        },
        trace: {
            type: 'dateFile',
            filename: 'logs/tracelog/',
            pattern: 'trace-yyyy-MM-dd.log',
            alwaysIncludePattern: true,
            daysToKeep: 3,
        },
        debug: {
            type: 'dateFile',
            filename: 'logs/debuglog/',
            pattern: 'debug-yyyy-MM-dd.log',
            alwaysIncludePattern: true,
            daysToKeep: 3,
        },
        info: {
            type: 'dateFile',
            filename: 'logs/infolog/',
            pattern: 'info-yyyy-MM-dd.log',
            alwaysIncludePattern: true,
            daysToKeep: 3,
        },
        warn: {
            type: 'dateFile',
            filename: 'logs/warnlog/',
            pattern: 'warn-yyyy-MM-dd.log',
            alwaysIncludePattern: true,
            daysToKeep: 3,
        },
        error: {
            type: 'dateFile',
            filename: 'logs/errorlog/',
            pattern: 'error-yyyy-MM-dd.log',
            alwaysIncludePattern: true,
            daysToKeep: 3,
        },
        fatal: {
            type: 'dateFile',
            filename: 'logs/fatallog/',
            pattern: 'fatal-yyyy-MM-dd.log',
            alwaysIncludePattern: true,
            daysToKeep: 3,
        },
    },
    categories: {
        trace: {appenders: ['stdout', 'trace'], level: 'trace', enableCallStack: true},//appenders:采用的appender,取appenders项,level:设置级别
        debug: {appenders: ['stdout', 'debug'], level: 'debug', enableCallStack: true},
        default: {appenders: ['stdout', 'info'], level: 'info', enableCallStack: true},
        warn: {appenders: ['stdout', 'warn'], level: 'warn', enableCallStack: true},
        error: {appenders: ['stdout', 'error'], level: 'error', enableCallStack: true},
        fatal: {appenders: ['stdout', 'fatal'], level: 'fatal', enableCallStack: true}
    }
})

exports.getLogger = function (name) {//name取categories项
    return log4js.getLogger(name || 'info')
}

exports.useLogger = function (app, logger) {//用来与express结合
    app.use(log4js.connectLogger(logger || log4js.getLogger('info'), {
        format: '[:remote-addr :method :url :status :response-timems][:referrer HTTP/:http-version :user-agent]'//自定义输出格式
    }))
}